#ifndef FICHERCLIENT_H
#define FICHERCLIENT_H
#include <vector>
#include "client.h"
#include "Particuliers.h"
#include "Professionel.h"
using namespace std;
class FicherClient
{
    public:
        FicherClient();
        virtual ~FicherClient();

        vector<client*> Getregistre() { return registre; }
        void NouveauClient(client* val) { registre.push_back (val); }
        void supprimerClient(unsigned int);
    protected:

    private:
        vector<client*> registre;
};

#endif // FICHERCLIENT_H
