#ifndef ADRESSEPOSTALE_H
#define ADRESSEPOSTALE_H
#include <iostream>
#include <cstring>
#include <ctype.h>
using namespace std;

class AdressePostale
{
    public:
        AdressePostale();
        AdressePostale(string,string,unsigned int,string);
        virtual ~AdressePostale();

        string GetLibelle() { return Libelle; }
        void SetLibelle(string val) { Libelle = val; }
        string GetComplement() { return Complement; }
        void SetComplement(string val) { Complement = val; }
        unsigned int GetCode_Postal() { return Code_Postal; }
        void SetCode_Postal(unsigned int val) { Code_Postal=val;}
        string GetVille();
        void SetVille(string val) { Ville = val; }

    protected:

    private:
        string Libelle;
        string Complement;
        unsigned int Code_Postal;
        string Ville;
};

#endif // ADRESSEPOSTALE_H
