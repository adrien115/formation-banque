#ifndef PROFESSIONEL_H
#define PROFESSIONEL_H
#include <cstring>
#include <ctype.h>
#include "client.h"
#include <algorithm>
#include <sstream>
#include <iomanip>
#include <vector>
using namespace std;

class Professionel : public client
{
    public:
        Professionel();
        virtual ~Professionel();

        unsigned long long int Getsiret() { return siret; }
        void Setsiret(unsigned long long int val) { siret = val; }
        char* GetRS() { return RS; }
        void SetRS(char val[50]) { for(int i=0;i<strlen(val);i++)RS[i]=val[i]; }
        unsigned short int GetAdC() { return AdC; }
        void SetAdC(unsigned short int val) { AdC = val; }
        //string GetAdPos() { return AdPos; }
        //void SetAdPos(AdressePostale val) { AdPos = val; }
        string Getmail(){return mail;};
        void Setmail(string val);
        string info() override;

    protected:

    private:
        unsigned long long int siret;
        char RS[50];
        unsigned short int AdC;
        //AdressePostale AdPos;
        string mail;
};

#endif // PROFESSIONEL_H
