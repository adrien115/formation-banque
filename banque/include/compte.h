#ifndef COMPTE_H
#define COMPTE_H
#include <iostream>
#include "registre.h"
#include <vector>
#include <cstring>
#include <ctype.h>
#include <algorithm>
#include <sstream>
#include <iomanip>

using namespace std;
class compte
{
    public:
        compte();
        compte(unsigned int numeroCompte=0,unsigned short int jour=1,unsigned short mois=1,unsigned short annee=1900,double solde=0,double decouAuto=0);
        virtual ~compte();

        unsigned short int * GetDoC() { return DoC; }
        void SetDoC(unsigned short int j,unsigned short int m,unsigned short int a) { DoC[0] = j;DoC[1] = m; DoC[2]=a; }
        unsigned int GetnumeroCompte() { return numeroCompte; }
        void SetnumeroCompte(unsigned int val) { numeroCompte = val; }
        double Getsolde() { return solde; }
        void Setsolde(double val) { solde = val; }
        double GetdecouvertAutorise() { return decouvertAutorise; }
        void SetdecouvertAutorise(double val) { decouvertAutorise = val; }
        void Setregistre( unsigned int date,unsigned short int codeOperation,unsigned int montant);
        void suprimerOperation(unsigned int ligne);
        vector<registre*> Getregistre(){return RegistreOperation;}
        void operator+(double);
        void operator-(double);

    protected:

    private:
        unsigned short int DoC[3];
        unsigned int numeroCompte;
        double solde;
        double decouvertAutorise;
        vector <registre*> RegistreOperation;

};

#endif // COMPTE_H
