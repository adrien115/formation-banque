#ifndef REGISTRE_H
#define REGISTRE_H


class registre
{
    public:
        registre(unsigned int date=0,unsigned short int codeOperation=1,double montant=0);
        virtual ~registre();

        unsigned int Getdate() { return date; }
        void Setdate(unsigned int val) { date = val; }
        unsigned short int GetcodeOperation() { return codeOperation; }
        void SetcodeOperation(unsigned short int val) { codeOperation = val; }
        double Getmontant() { return montant; }
        void Setmontant(double val) { montant = val; }

    protected:

    private:
        unsigned int date;
        unsigned short int codeOperation;
        double montant;
};

#endif // REGISTRE_H
