#ifndef PARTICULIERS_H
#define PARTICULIERS_H
#include <iostream>
using namespace std;
#include <sstream>
#include <string>
#include <iomanip>
#include <client.h>


class Particuliers : public client
{
    public:
        Particuliers();
        virtual ~Particuliers();

        char Getsituation_familiale() { return situation_familiale; }
        void Setsituation_familiale(char val);
        unsigned short int * GetDdN() { return DdN; }
        void SetDdN(int j,int m,int a) {DdN[0]=j;DdN[1]=m;DdN[2]=a;}
        string info() override;
    protected:

    private:
        char situation_familiale;
        unsigned short int DdN[3];
};

#endif // PARTICULIERS_H
