#ifndef CLIENT_H
#define CLIENT_H
#include <iostream>
using namespace std;
#include"AdressePostale.h"
#include <cstring>
#include <ctype.h>
#include <vector>
#include <algorithm>
#include <cstring>
#include <sstream>
#include <iomanip>
#include "compte.h"

class client
{
    public:
        client();
        client(char nom[50],char prenom[50],unsigned int numeroClient,char sexe, unsigned short int v1,string libelle,string complement,unsigned int codepostale,string ville);

        virtual ~client();

        char*  Getnom() { return nom; }
        void Setnom(char  val[50]);
        char * Getprenom() { return prenom; }
        void Setprenom(char  val[50]);
        char Getsexe() { return sexe; }
        void Setsexe(char val);
        unsigned short int * Gettelephone() { return telephone; }
        void Settelephone(unsigned short int v1,unsigned short int v2,unsigned short int v3,unsigned short int v4,unsigned short int v5) {telephone[0]=v1; telephone[1]=v2;telephone[2]=v3;telephone[3]=v4;telephone[4]=v5;}
        virtual string info()=0;
        void Setadress(string libelle,string complement,unsigned int codepostale,string ville);
        int Getnbcompte(){return (c.size());}
        void SetCompte(int numero,unsigned short jour,unsigned short mois, unsigned short annee,int solde,int decouAuto);
        compte* GetCompteParNu(unsigned int);
        string Getcompte();
        unsigned int GetNumeroClient(){return NumeroClient;}
        void SetNumeroClient(unsigned int val){NumeroClient=val;}

    protected:
        AdressePostale AdPos;
    private:
        unsigned int NumeroClient;
        char  nom[50];
        char  prenom[50];
        char sexe;
        unsigned short int  telephone[5];
        vector<compte*>c;

};

#endif // CLIENT_H
