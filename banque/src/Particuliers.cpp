#include "Particuliers.h"

Particuliers::Particuliers()
{
    Setsituation_familiale('X');
    SetDdN(0,0,1900);
}

Particuliers::~Particuliers()
{
    //dtor
}
void Particuliers::Setsituation_familiale(char val)
{
    val=toupper(val);
    if(val!='C'&&val!='M'&&val!='D'&&val!='X')
        throw "choix de la situation familiale incorrect C : C�libataire, M : Mari�(e), D : Divorc�(e), X : Autre ";
    else
        situation_familiale=val;
}
string Particuliers::info()
{
    ostringstream oss;
    oss<<"Particulier : "<<setfill ('0') << setw (5)<<GetNumeroClient()<<endl;
    if(Getsexe()=='M')
        oss<<"M.";
    else
        oss<<"Mme.";
    oss<<Getnom()<<" "<<Getprenom()<<endl<<AdPos.GetLibelle()<<","<<AdPos.GetComplement()<<endl<< setfill ('0') << setw (5)<<AdPos.GetCode_Postal()<<" "<<AdPos.GetVille()<<endl;
    oss<<endl<<"telephone : ";
    for(int i=0;i<5;i++)
        oss<<setfill ('0') << setw (2)<<Gettelephone()[i]<<" ";
    oss<<endl<<"Age : "<<2020-GetDdN()[2]<<endl;
    return oss.str();

}
